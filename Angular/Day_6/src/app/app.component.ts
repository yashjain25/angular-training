import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Day_6';
  validLink:any="https://www.youtube.com/"
  updatedLink:any="https://www.google.com/"
}
