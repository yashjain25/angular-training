var Scientific = /** @class */ (function () {
    function Scientific(display, buttons) {
        this.initialvalue = ' ';
        this.display = display;
        this.buttons = buttons;
    }
    Scientific.prototype.operation = function () {
        var _this = this;
        var _loop_1 = function (i) {
            buttons[i].addEventListener('click', function () {
                _this.buttontext = buttons[i].innerHTML;
                switch (_this.buttontext) {
                    case "X":
                        _this.buttontext = "*";
                        _this.initialvalue = _this.initialvalue + _this.buttontext;
                        _this.display.value = _this.initialvalue;
                        break;
                    case "C":
                        _this.initialvalue = " ";
                        _this.display.value = _this.initialvalue;
                        break;
                    case "=":
                        _this.display.value = eval(_this.initialvalue);
                        break;
                    default:
                        _this.initialvalue += _this.buttontext;
                        _this.display.value = _this.initialvalue;
                }
            });
        };
        for (var i = 0; i < buttons.length; i++) {
            _loop_1(i);
        }
    };
    return Scientific;
}());
var display = document.getElementById('print');
var buttons = document.getElementsByClassName("inc");
var Scientificobject = new Scientific(display, buttons);
Scientificobject.operation();
