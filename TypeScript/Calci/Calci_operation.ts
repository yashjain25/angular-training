class Scientific{
    display:HTMLInputElement
    buttons:HTMLCollection
    initialvalue:string=' '
    buttontext:string
    constructor(display:HTMLInputElement,buttons:HTMLCollection){
        this.display=display
        this.buttons=buttons
    }

    operation():void{
    for(let i:number=0;i<buttons.length;i++)
    {
        buttons[i].addEventListener('click',() =>
    {
        this.buttontext=buttons[i].innerHTML
        switch(this.buttontext){
            case "X":
                this.buttontext="*"
                this.initialvalue =this.initialvalue+this.buttontext;
                this.display.value =this.initialvalue
                break
            case "C":
                this.initialvalue=" ";
                this.display.value=this.initialvalue
                break
            case "=":
                this.display.value=eval(this.initialvalue)
                break
            default:
                this.initialvalue+=this.buttontext;
                this.display.value=this.initialvalue
        }
    })
}

}
}
const display=document.getElementById('print') as HTMLInputElement
const buttons=document.getElementsByClassName("inc") as HTMLCollection
const Scientificobject=new Scientific(display,buttons)
Scientificobject.operation()