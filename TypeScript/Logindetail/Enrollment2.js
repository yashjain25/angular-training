var Sign = /** @class */ (function () {
    function Sign(star, password, submitButton, registerPath) {
        this.star = star;
        this.password = password;
        this.submitButton = submitButton;
        this.registerPath = registerPath;
    }
    Sign.prototype.storing = function () {
        var _this = this;
        star.addEventListener('input', function (e) {
            _this.names = e.target.value;
        });
        password.addEventListener('input', function (ev) {
            _this.passes = ev.target.value;
        });
        submitButton.addEventListener('click', function () {
            localStorage.setItem("Name", _this.names);
            localStorage.setItem("Password", _this.passes);
        });
    };
    Sign.prototype.track = function () {
        registerPath.addEventListener('click', function () {
            window.location.href = "Login.html";
        });
    };
    return Sign;
}());
var star = document.getElementById('uname');
var password = document.getElementById('upass');
var submitButton = document.getElementById('register');
var registerPath = document.getElementById('navigate-login');
var signObject = new Sign(star, password, submitButton, registerPath);
signObject.storing();
signObject.track();
