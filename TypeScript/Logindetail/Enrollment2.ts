class Sign{
    star:HTMLInputElement
    password:HTMLInputElement
    submitButton:HTMLInputElement
    registerPath:HTMLInputElement
    names:string
    passes:string
    constructor(star:HTMLInputElement,password:HTMLInputElement,submitButton:HTMLInputElement,registerPath:HTMLInputElement){
        this.star=star
        this.password=password
        this.submitButton=submitButton
        this.registerPath=registerPath
    }
    
    storing():void{
    star.addEventListener('input',(e) => {
        this.names=(e.target as HTMLInputElement).value
    
    })
    password.addEventListener('input',(ev) => {
        this.passes=(ev.target as HTMLInputElement).value
    })
    submitButton.addEventListener('click',() => {
    
        localStorage.setItem("Name",this.names)
        localStorage.setItem("Password",this.passes)
    })}
    track():void{
    registerPath.addEventListener('click',() => {
        window.location.href="Login.html"
})
}
}
const star=document.getElementById('uname') as HTMLInputElement
const password=document.getElementById('upass') as HTMLInputElement
const submitButton=document.getElementById('register') as HTMLInputElement
const registerPath=document.getElementById('navigate-login') as HTMLInputElement
const signObject=new Sign(star,password,submitButton,registerPath)
signObject.storing()
signObject.track()
