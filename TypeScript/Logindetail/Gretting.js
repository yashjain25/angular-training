var Images = document.getElementsByClassName('astra');
var caption = document.getElementsByClassName('carousel-caption');
var dataArray = new Array();
fetch("data.json")
    .then(function (response) {
    if (!response.ok) {
        throw new Error("HTTP error " + response.status);
    }
    return response.json();
})
    .then(function (json) {
    dataArray[0] = json.emp1;
    dataArray[1] = json.emp2;
    dataArray[2] = json.emp3;
    dataArray[3] = json.emp4;
})["catch"](function () {
    this.dataError = true;
});
var _loop_1 = function (i) {
    Images[i].addEventListener('click', function () {
        var createElements = document.createElement('p');
        var texts = document.createTextNode("Name: ".concat(dataArray[i].Name, " \n Designation: ").concat(dataArray[i].Designation, " \n Project: ").concat(dataArray[i].Project));
        createElements.appendChild(texts);
        caption[i].appendChild(createElements);
    });
};
for (var i = 0; i < Images.length; i++) {
    _loop_1(i);
}
