var Approval = /** @class */ (function () {
    function Approval(nameField, passField, signButton, registerButton) {
        this.nameField = nameField;
        this.passField = passField;
        this.signButton = signButton;
        this.registerButton = registerButton;
    }
    Approval.prototype.check = function () {
        var _this = this;
        nameField.addEventListener('input', function (e) {
            _this.names = e.target.value;
        });
        passField.addEventListener('input', function (ev) {
            _this.passes = ev.target.value;
        });
        var prevName = localStorage.getItem('Name');
        var prevPass = localStorage.getItem('Password');
        signButton.addEventListener('click', function () {
            console.log(prevName);
            console.log(prevPass);
            if ((prevName == _this.names && prevPass == _this.passes)) {
                window.location.href = 'Gretting.html';
            }
            else {
                alert("Invalid User");
            }
        });
    };
    Approval.prototype.ride = function () {
        registerButton.addEventListener('click', function () {
            window.location.href = "Enrollment.html";
        });
    };
    return Approval;
}());
var nameField = document.getElementById('username');
var passField = document.getElementById('userpassword');
var signButton = document.getElementById('signbutton');
var registerButton = document.getElementById('register-navigation');
var approvalObject = new Approval(nameField, passField, signButton, registerButton);
approvalObject.check();
approvalObject.ride();
