var Second = /** @class */ (function () {
    function Second(button) {
        this.button = button;
    }
    Second.prototype.perform = function () {
        this.button.addEventListener('click', function () {
            alert("This is an Assignment 1");
        });
    };
    return Second;
}());
var clickMe = document.getElementById("btm1");
var secondObject = new Second(clickMe);
secondObject.perform();
