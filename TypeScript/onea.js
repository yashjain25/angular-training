var Assignone = /** @class */ (function () {
    function Assignone(element, sElement) {
        this.element = element;
        this.sElement = sElement;
    }
    Assignone.prototype.help = function () {
        var _this = this;
        this.clear = setInterval(function () {
            if (_this.sElement > 0) {
                _this.sElement -= 1;
                if (_this.sElement % 2 == 0) {
                    element.style.background = "#999966";
                }
                else {
                    element.style.background = "#d6d6c2";
                }
            }
            else {
                _this.sElement = 0;
                element.style.background = "#ff8533";
            }
            element.innerText = _this.sElement;
            if (_this.sElement < 0) {
                clearInterval(_this.clear);
            }
        }, 1000);
    };
    return Assignone;
}());
var element = document.getElementById("Timerid");
var sElement = element.innerText;
var assign = new Assignone(element, sElement);
assign.help();
